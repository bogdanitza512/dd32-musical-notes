import React from 'react';
import { render } from '@testing-library/react';

import { Inspector } from './inspector';

describe(' NoteInspector', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Inspector />);
    expect(baseElement).toBeTruthy();
  });
});
