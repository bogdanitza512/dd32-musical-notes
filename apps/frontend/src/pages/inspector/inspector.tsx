import React, { useCallback } from 'react';

import styled from 'styled-components';

import BackgroundURL from '../../assets/inspector-background.svg';
import NoteViewURL from '../../assets/note-view.svg';
import PlayNoteURL from '../../assets/play-note.svg';

import notes from '../../assets/notes';
import { useHistory } from 'react-router-dom';

import UIFx from 'uifx';
import { useLocalStore } from '../../hooks/useLocalStore';

/* eslint-disable-next-line */
export interface InspectorProps {}

const StyledInspector = styled.div`
  /* Background stuff */
  background-image: url(${BackgroundURL});
  background-repeat: no-repeat;
  background-position: top;
  background-size: contain;
  /* Fullscreen stuff */
  position: fixed;
  height: 100%;
  width: 100%;
  left: 0;
  top: 0;
  /* Flexbox stuff */
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
`;

const StyledNoteView = styled.div`
  /* Background stuff */
  background-image: url(${NoteViewURL});
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
  /* Sizing stuff */
  position: relative;
  width: 90vmin;
  height: 90vmin;
  z-index: 1;
`;

const getStyledNote = (index: number) =>
  styled(notes[index].svg)`
    position: absolute;
    width: 90%;
    height: 90%;
    z-index: 2;
    margin: auto;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    path {
      stroke: ${props => props.theme.note.color.active};
      fill: ${props => props.theme.note.color.active};
    }
  `;

const StyledPlayButton = styled.button`
  position: relative;
  z-index: 0;
  margin-left: auto;
  margin-right: auto;
  width: 35vmin;
  height: 15vmin;
  left: 0;
  right: 0;
  bottom: 2vmax;
  padding-left: 10vmin;
  background-image: url(${PlayNoteURL});
  background-color: transparent;
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
  border: none;
  color: white;
  font-size: 1.25rem;
`;

const StyledText = styled.span`
  font-size: 1.5rem;
  color: #00bfd6;
  width: 85%;
`;

const StyledButton = styled.button`
  background-color: #00bfd6;
  border: none;
  color: white;
  padding: 5% 10%;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 1.25rem;
  border-radius: 60px;
  margin-top: 5vmax;
  margin-bottom: 5vmax;
`;

export const Inspector = (_props: InspectorProps) => {
  const [store] = useLocalStore();
  const history = useHistory();

  const StyledNote = getStyledNote(store.id - 1);

  const handlePlayClick = useCallback(() => {
    const index = store.id - 1;
    const note = notes[index];
    const sound = new UIFx(note.mp3);
    sound.play();
  }, [store]);

  const handleSendClick = useCallback(() => {
    history.push('/stave');
  }, [history]);

  return (
    <StyledInspector>
      <div>
        <StyledNoteView>
          <StyledNote />
        </StyledNoteView>
        <StyledPlayButton onClick={handlePlayClick}>Redă</StyledPlayButton>
      </div>
      <StyledText>
        Te vom anunța când violoncelistul cântă fragmentul tău!
      </StyledText>
      <StyledButton onClick={handleSendClick}>Trimite pe portativ</StyledButton>
    </StyledInspector>
  );
};

export default Inspector;
