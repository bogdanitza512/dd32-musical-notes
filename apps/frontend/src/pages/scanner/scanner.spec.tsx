import React from 'react';
import { render } from '@testing-library/react';

import { Scanner } from './scanner';

describe(' QRScanner', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Scanner />);
    expect(baseElement).toBeTruthy();
  });
});
