import React, { useCallback, useState, useEffect } from 'react';

import styled from 'styled-components';
import QrReader from 'react-qr-reader';

import useWindowSize from '../../hooks/useWindowSize';

import MaskURL from '../../assets/mask.svg';
import { ReactComponent as Reticle } from '../../assets/reticle.svg';
import { ReactComponent as OlumiantLogo } from '../../assets/logo-olumiant.svg';
import { useHistory } from 'react-router-dom';
import { useLocalStore } from '../../hooks/useLocalStore';
import { actions } from '../../reducers/localStore';
import { getRandomNoteFromSong } from '../../common/utils';
import { mutate, increment } from '../../hooks/useDistributedCounter';

/* eslint-disable-next-line */
export interface ScannerProps {}

const StyledScanner = styled.div`
  /* color: pink; */
  /* position: fixed; */
  height: 100%;
  width: 100%;
`;

const StyledQRReader = styled(QrReader)`
  position: absolute;
  video {
    position: fixed !important;
    overflow: hidden;
    width: 100%;
    height: 100%;
    z-index: 1;
  }
`;

const StyledViewFinder = styled.div`
  overflow: hidden;
  position: fixed;
  height: 100%;
  width: 100%;
  z-index: 10;
  left: 0;
  top: 0;
  mask: url(${MaskURL});
  background-color: rgba(0, 0, 0, 0.65);
  width: 100%;
  height: 100%;
`;

const FlexContainer = styled.div`
  position: absolute;
  left: calc(50% - 15vmax);
  top: calc(30% - 15vmax);
  width: 30vmax;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  align-items: center;
  align-content: center;
`;

const StyledReticle = styled(Reticle)`
  width: 30vmax;
  height: 30vmax;
`;

const StyledLogo = styled(OlumiantLogo)`
  /* width: 50vmin; */
  margin-top: 5vmax;
`;

const StyledButton = styled.button`
  margin-top: 5vmax;
  background-color: black;
  border: none;
  color: #999999;
  padding: 10% 15%;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 1em;
  border-radius: 60px;
`;

export const Scanner = (props: ScannerProps) => {
  const { width, height } = useWindowSize();
  const history = useHistory();
  const [store, dispatch] = useLocalStore();

  useEffect(() => dispatch(actions.updatePerformedScan(false)), [dispatch]);

  const handleScan = useCallback(
    (data: string) => {
      if (data && !store.performedScan) {
        const id = getRandomNoteFromSong().uid;
        dispatch(actions.updatePerformedScan(true));
        dispatch(actions.updateNoteID(id));
        console.log('QR detected');
      }
    },
    [store, dispatch]
  );
  const handleError = useCallback((err: any) => console.error(err), []);

  const handleClick = useCallback(() => {
    if (store.performedScan) {
      increment('scoreboard/counter');
      history.push('/inspector');
    }
  }, [store, history]);

  return (
    <StyledScanner>
      <StyledQRReader
        showViewFinder={false}
        resolution={Math.max(width, height)}
        facingMode={'environment'}
        onError={handleError}
        onScan={handleScan}
      />

      <StyledViewFinder>
        <FlexContainer>
          <StyledReticle />
          <StyledLogo />
          <StyledButton onClick={handleClick}>Scanează codul QR</StyledButton>
        </FlexContainer>
      </StyledViewFinder>
    </StyledScanner>
  );
};

export default Scanner;
