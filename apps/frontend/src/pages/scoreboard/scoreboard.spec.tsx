import React from 'react';
import { render } from '@testing-library/react';

import Scoreboard from './scoreboard';

describe(' Scoreboard', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Scoreboard />);
    expect(baseElement).toBeTruthy();
  });
});
