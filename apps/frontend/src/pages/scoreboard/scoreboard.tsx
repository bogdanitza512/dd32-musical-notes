import React, { useEffect, useState } from 'react';

import styled from 'styled-components';
import stave from '../../assets/stave.svg';
import Note from '../../components/note/note';

import song from '../../assets/song.json';
import BottomSheet from '../../components/bottom-sheet/bottom-sheet';
import { useKeyPress } from '../../hooks/useKeyPress';
import { useDistributedCounter } from '../../hooks/useDistributedCounter';

import _ from 'lodash-es';
import { motion } from 'framer-motion';
import { useDocumentState } from '../../hooks/useDocumentState';
import UIFx from 'uifx';

/* eslint-disable-next-line */
export interface ScoreProps {
  // readonly data: Array<{note: number, offset: number}>;
}

const StyledScore = styled.div`
  min-width: 60%;
  max-width: 90%;
  margin: 5% 5%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: right;
  background-image: url(${stave});
  background-size: ${props => props.theme.utils.stave.size};
  margin-bottom: 10vh;
`;

const Content = styled.div`
  border-radius: 10px;
  user-select: none;
  background: rgb(248, 124, 34);
  color: white;
  width: 100%;
  height: 300%;
  /* padding: 1em 1em 4em 1em; */
  display: flex;
  flex-direction: column;
  cursor: pointer;
`;

const Header = styled.div`
  margin-top: 1vh;
  margin-bottom: 1vh;
  display: flex;
  justify-content: center;
  align-items: center;
  vertical-align: middle;
  max-height: 10vh;
  min-height: 10vh;
`;

const Section = styled.div`
  height: 100%;
  overflow: auto;
  margin: 0;
  padding: 0;
  max-height: 90vh;
  min-height: 90vh;
`;

const ListItem = styled.li`
  list-style: none;
  margin: 0;
  height: 3em;
  border-bottom: 1px solid white;
`;

const stagger = song.duration / song.score.length;

const child = {
  start: {
    scale: [1, 1.25, 1.25, 1]
  },
  end: {
    scale: 1
  }
};

const transition = {
  duration: stagger,
  ease: 'easeInOut'
};

const AnimatedNote = motion.custom(Note);

export const Scoreboard = (_props: ScoreProps) => {
  // const [store, dispatch] = useLocalStore();
  const [counter, increment] = useDistributedCounter('scoreboard/counter', 10);

  const plus_press = useKeyPress('+');
  const minus_press = useKeyPress('-');
  const equal_press = useKeyPress('=');
  const one_press = useKeyPress('1');
  const zero_press = useKeyPress('0');

  const [actives, mutate] = useState([]);
  const [state, set] = useDocumentState('scoreboard/state');

  //const [music] = useState(new UIFx('./assets/song.mp3'));

  useEffect(() => {
    if (equal_press) {
      console.log(counter);
    }
    if (plus_press) {
      if (counter + 1 <= song.score.length) {
        increment(1);
      }
    }
    if (minus_press) {
      if (counter - 1 >= 0) {
        increment(-1);
      }
    }
    if (one_press) {
      increment(song.score.length - counter);
    }
    if (zero_press) {
      increment(-counter);
    }
  }, [equal_press, plus_press, minus_press, one_press, zero_press]);

  useEffect(() => {
    if (counter > actives.length) {
      const delta = counter - actives.length;
      const indexes = _.range(0, song.score.length);
      const inactives = _.difference(indexes, actives);
      const shuffled = _.shuffle(inactives);
      const selection = _.take(shuffled, delta);
      const mutation = _.concat(actives, selection);
      mutate(mutation);
    } else if (counter < actives.length) {
      const delta = actives.length - counter;
      const shuffled = _.shuffle(actives);
      const selection = _.take(shuffled, delta);
      const mutation = _.difference(actives, selection);
      mutate(mutation);
    }
    if (counter == 0) {
      set({ toggle: false });
    }
    if (counter == song.score.length) {
      set({ toggle: true });
      const music = new UIFx('./assets/song.mp3');
      music.play();
    }
  }, [counter]);

  return (
    <StyledScore>
      {song.score.map((item, index) => (
        <AnimatedNote
          animate={actives.includes(index) ? 'start' : 'end'}
          variants={child}
          transition={transition}
          uid={item.uid}
          offset={item.offset}
          key={`mn_${item.uid}_${index}`}
          active={actives.includes(index)}
        />
      ))}
      <BottomSheet height={100}>
        <Content>
          <Header>
            <div>Te vom anunţa când violoncelistul cântă fragmentul tău!</div>
          </Header>
          <Section>
            {Array(8)
              .fill(0)
              .map((_, i) => (
                <ListItem key={i} />
              ))}
          </Section>
        </Content>
      </BottomSheet>
    </StyledScore>
  );
};

export default Scoreboard;
