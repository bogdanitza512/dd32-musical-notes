import React from 'react';
import { render } from '@testing-library/react';

import Stave from './stave';

describe(' Stave', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Stave />);
    expect(baseElement).toBeTruthy();
  });
});
