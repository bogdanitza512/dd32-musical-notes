import React, { useEffect, useState } from 'react';

import styled from 'styled-components';
import stave from '../../assets/stave.svg';
import Note from '../../components/note/note';

import song from '../../assets/song.json';
import { useLocalStore } from '../../hooks/useLocalStore';
import BottomSheet from '../../components/bottom-sheet/bottom-sheet';
import { useKeyPress } from '../../hooks/useKeyPress';
import { motion } from 'framer-motion';
import { useDocumentState } from '../../hooks/useDocumentState';
import { notes } from '../../assets/notes';
import UIFx from 'uifx';

/* eslint-disable-next-line */
export interface ScoreProps {
  // readonly data: Array<{note: number, offset: number}>;
}

const StyledScore = styled(motion.div)`
  min-width: 60%;
  max-width: 90%;
  margin: 5% 5%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: right;
  background-image: url(${stave});
  background-size: ${props => props.theme.utils.stave.size()};
  margin-bottom: 10vh;
`;

const Content = styled.div`
  border-radius: 10px;
  user-select: none;
  background: rgb(248, 124, 34);
  color: white;
  width: 100%;
  height: 300%;
  /* padding: 1em 1em 4em 1em; */
  display: flex;
  flex-direction: column;
  cursor: pointer;
`;

const Header = styled.div`
  margin-top: 1vh;
  margin-bottom: 1vh;
  display: flex;
  justify-content: center;
  align-items: center;
  vertical-align: middle;
  max-height: 10vh;
  min-height: 10vh;
`;

const Section = styled.div`
  height: 100%;
  overflow: auto;
  margin: 0;
  padding: 0;
  max-height: 90vh;
  min-height: 90vh;
`;

const ListItem = styled.li`
  list-style: none;
  margin: 0;
  height: 3em;
  border-bottom: 1px solid white;
`;

const stagger = song.duration / song.score.length;

const container = {
  start: {
    transition: {
      staggerChildren: stagger
    }
  },
  end: {
    transition: {
      staggerChildren: stagger / 2
    }
  }
};

const child = {
  start: {
    scale: [1, 1.25, 1.25, 1]
  },
  end: {
    scale: 1
  }
};

const transition = {
  duration: stagger,
  ease: 'easeInOut'
};

const AnimatedNote = motion.custom(Note);

export const Stave = (_props: ScoreProps) => {
  const [store] = useLocalStore();
  const z_pressed = useKeyPress('z');
  const [state, set] = useDocumentState('scoreboard/state');

  useEffect(() => {
    if (z_pressed) {
      set({ toggle: !state.toggle });
    }
  }, [z_pressed]);

  useEffect(() => {
    if (state && state.toggle === true) {
      const timer = setTimeout(() => {
        console.log('buzzz');
        window.navigator.vibrate(200);
        const index = store.id - 1;
        const note = notes[index];
        const sound = new UIFx(note.mp3);
        sound.play();
      }, stagger * (store.id - 1) * 1000);
      return () => clearTimeout(timer);
    }
  }, [state]);

  return (
    <StyledScore
      initial={false}
      variants={container}
      animate={state && state.toggle === true ? 'start' : 'end'}
    >
      {song.score.map((item, index) => (
        <AnimatedNote
          variants={child}
          transition={transition}
          uid={item.uid}
          offset={item.offset}
          key={`mn_${item.uid}_${index}`}
          active={index === store.id - 1}
        />
      ))}
      <BottomSheet height={100}>
        <Content>
          <Header>
            <div>Te vom anunţa când violoncelistul cântă fragmentul tău!</div>
          </Header>
          <Section>
            {Array(8)
              .fill(0)
              .map((_, i) => (
                <ListItem key={i} />
              ))}
          </Section>
        </Content>
      </BottomSheet>
    </StyledScore>
  );
};

export default Stave;
