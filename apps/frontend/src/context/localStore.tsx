import data from '../assets/store.json';
import React, { Dispatch, createContext, useReducer, useEffect } from 'react';
import { Action, reducer } from '../reducers/localStore';

export const initial = { ...data };

export type Store = typeof initial;
export type Content = [Store, Dispatch<Action>];

export const Context = createContext({} as Content);

export const local = {
  key: 'local-store',
  get: () => JSON.parse(localStorage.getItem(local.key) as string) as Store,
  set: (store: Store) => localStorage.setItem(local.key, JSON.stringify(store))
};

export const createLocalStore = () => {
  const [store, dispatch] = useReducer(reducer, local.get() || initial);
  useEffect(() => local.set(store), [store]);
  return [store, dispatch] as Content;
};

export const LocalStore = ({ children }) => {
  const payload = createLocalStore();
  return <Context.Provider value={payload}>{children}</Context.Provider>;
};
