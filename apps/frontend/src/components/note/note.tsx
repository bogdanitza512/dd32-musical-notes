import React, { forwardRef, Ref } from 'react';
import styled from 'styled-components';
import notes from '../../assets/notes';

/* eslint-disable-next-line */
export interface NoteProps {
  uid: number;
  offset: number;
  active: boolean;
}

const styledNotes = notes.map(
  ({ svg }) => styled(svg)<{ offset: number; active: number }>`
    width: ${props => props.theme.utils.note.width()};
    height: ${props => props.theme.utils.note.height()};
    transform: translateY(
      ${props => props.theme.utils.note.offset(props.offset)}
    );
    path {
      stroke: ${props => props.theme.utils.note.color(!!props.active)};
      fill: ${props => props.theme.utils.note.color(!!props.active)};
    }
  `
);
const StyledNote = styled.span`
  display: inline-block;
  width: ${props => props.theme.utils.note.width()};
  height: ${props => props.theme.utils.note.height()};
`;

export const Note = forwardRef((props: NoteProps, ref: Ref<HTMLDivElement>) => {
  const StyledSVG = styledNotes[props.uid - 1];
  return (
    <StyledNote ref={ref}>
      <StyledSVG active={props.active ? 1 : 0} offset={props.offset} />
    </StyledNote>
  );
});

export default Note;
