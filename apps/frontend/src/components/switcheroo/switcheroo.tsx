import React, { useContext } from 'react';

import {
  useLocation,
  useHistory,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';

// import { useTransition, animated } from 'react-spring';
import { Context as LocalStore } from '../../context/localStore';
import Stave from '../../pages/stave/stave';
import Scoreboard from '../../pages/scoreboard/scoreboard';
import Inspector from '../../pages/inspector/inspector';
import Scanner from '../../pages/scanner/scanner';

export const Swicheroo = () => {
  const [store] = useContext(LocalStore);
  const location = useLocation();
  const history = useHistory();

  // prevent back navigation
  history.listen((newLocation, action) => {
    if (action === 'PUSH') {
      if (
        newLocation.pathname !== location.pathname ||
        newLocation.search !== location.search
      ) {
        // Save new location
        location.pathname = newLocation.pathname;
        location.search = newLocation.search;

        // Clone location object and push it to history
        history.push({
          pathname: newLocation.pathname,
          search: newLocation.search
        });
      }
    } else {
      // Send user back if they try to navigate back
      history.go(1);
    }
  });

  // const transitions = useTransition(location, location => location.pathname, {
  //   from: { opacity: 0 },
  //   enter: { opacity: 1 },
  //   leave: { opacity: 0 }
  // });

  return (
    // <>
    // {
    //   transitions.map(
    //     ({ item, props, key }) => (
    //       <animated.div key={key} style={props}>
    <Switch /*location={item}*/>
      <Route exact path="/">
        {store.performedScan ? (
          <Redirect to="/inspector" />
        ) : (
          <Redirect to="/scanner" />
        )}
      </Route>
      <Route exact path="/scanner" component={Scanner} />
      <Route exact path="/inspector" component={Inspector} />
      <Route exact path="/stave" component={Stave} />
      <Route exact path="/scoreboard" component={Scoreboard} />
      <Redirect to="/" />
    </Switch>
    //       </animated.div>
    //     )
    //   )
    // }
    // </>
  );
};

export default Swicheroo;
