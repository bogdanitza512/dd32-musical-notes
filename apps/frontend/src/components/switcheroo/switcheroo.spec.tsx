import React from 'react';
import { render } from '@testing-library/react';

import Switcheroo from './switcheroo';

describe(' Switcheroo', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Switcheroo />);
    expect(baseElement).toBeTruthy();
  });
});
