import React, { useState, useEffect } from 'react';
import { animated, useSpring, config } from 'react-spring';
import { useGesture } from 'react-use-gesture';
import clamp from 'lodash-es/clamp';

import styled from 'styled-components';

type Mode = 'collapsed' | 'expanded';

/* eslint-disable-next-line */
interface Props {
  height: number;
  initial?: Mode;
  mode?: Mode;
  onModeChange?: (mode: Mode) => void;
  style?: React.CSSProperties;
}

const getY = (height: number, mode: Mode): number =>
  mode === 'collapsed' ? height * 0.9 : height * 0.1;

export const Style = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  pointer-events: none;
  z-index: 1;
`;

export const BottomSheet: React.FC<Props> = ({
  children,
  height,
  initial = 'collapsed',
  style,
  onModeChange
}) => {
  const [mode, setMode] = useState(initial);
  const [{ y }, set] = useSpring(() => ({
    y: getY(height, mode),
    config: config.stiff
  }));
  const collapsedY = getY(height, 'collapsed');
  const expandedY = getY(height, 'expanded');

  useEffect(() => {
    set({ y: mode === 'collapsed' ? collapsedY : expandedY });
  }, [mode, collapsedY, expandedY, set]);

  useEffect(() => {
    if (onModeChange) {
      onModeChange(mode);
    }
  }, [mode, onModeChange]);

  const calculateNextY = (
    down: boolean,
    currentY: number,
    deltaY: number,
    velocity: number
  ) => {
    if (down) {
      return currentY + deltaY;
    }

    const threshold = 100 / velocity;

    if (mode === 'expanded') {
      return deltaY > threshold ? collapsedY : expandedY;
    }

    if (mode === 'collapsed') {
      return deltaY < -threshold ? expandedY : collapsedY;
    }

    return getY(height, mode);
  };

  const bind = useGesture({
    onDrag: ({ down, delta: [_, deltaY], velocity }) => {
      // velocity = clamp(velocity, 1, 8);
      // const temp = y.getValue();
      // const threshold = 100 / velocity;
      // if (!down) {
      //   if (mode === 'expanded' && deltaY > threshold) {
      //     setMode('collapsed');
      //   } else if (mode === 'collapsed' && deltaY < -threshold) {
      //     setMode('expanded');
      //   }
      // }
      // const nextY = calculateNextY(down, temp, deltaY, velocity);
      // set({
      //   y: nextY,
      //   config: {
      //     mass: velocity,
      //     tension: 500 * velocity,
      //     friction: 50
      //   }
      // });
      // return temp;
    },
    onHover: ({ hovering }) => {
      // if (mode === 'collapsed') {
      //   set({
      //     y: hovering ? getY(height, mode) - 10 : getY(height, mode)
      //   });
      // }
    }
    // onMouseDown: () => setMode(mode === 'collapsed' ? 'expanded' : 'collapsed')
  });

  return (
    <Style>
      <animated.div
        {...bind()}
        style={{
          pointerEvents: 'all',
          height: `${height}vh`,
          transform: y.interpolate(y => `translateY(calc(${y}vh))`),
          ...style
        }}
      >
        {children}
      </animated.div>
    </Style>
  );
};

export default BottomSheet;
