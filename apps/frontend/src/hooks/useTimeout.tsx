import React, { useState, useEffect, useRef } from 'react';

export const useTimeout = (callback: () => void, delay: number) => {
  const savedCallback = useRef(null);

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the timeout.
  useEffect(() => {
    const tick = () => savedCallback.current();

    if (delay !== null) {
      const id = setTimeout(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
};
