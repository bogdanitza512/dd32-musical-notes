import { db } from '../services/firebase';
import firebase from 'firebase/app';
import 'firebase/firestore';
import { useState, useEffect } from 'react';

// export const create = (path: string, num_shards: number) => {
//   const ref = db.doc(path);
//   const batch = db.batch();
//   batch.set(ref, { num_shards: num_shards });
//   for (let i = 0; i < num_shards; i++) {
//     const shared = ref.collection('shards').doc(i.toString());
//     batch.set(shared, { count: 0 });
//   }
//   return batch.commit();
// };

// const get = async (path: string) => {
//   const ref = db.doc(path);
//   return ref
//     .collection('shards')
//     .get()
//     .then(snapshot => {
//       let total_count = 0;
//       snapshot.forEach(doc => {
//         total_count += doc.data().count;
//       });
//       return total_count;
//     });
// };

export const subscribe = (path: string, callback: (total: number) => void) => {
  const ref = db.doc(path);

  return ref.collection('shards').onSnapshot(snapshot => {
    let total = 0;
    snapshot.forEach(doc => {
      total += doc.data().count;
    });
    callback(total);
  });
};

export const mutate = (path: string, num_shards: number, amount: number) => {
  const ref = db.doc(path);
  const shard_id = Math.floor(Math.random() * num_shards).toString();
  const shard_ref = ref.collection('shards').doc(shard_id);
  const mutation = firebase.firestore.FieldValue.increment(amount);
  return shard_ref.update('count', mutation);
};

export const increment = async (path: string, amount: number = 1) => {
  const ref = db.doc(path);
  const num_shards = await ref
    .get()
    .then(doc => doc.data().num_shards as number);
  const shard_id = Math.floor(Math.random() * num_shards).toString();
  const shard_ref = ref.collection('shards').doc(shard_id);
  const mutation = firebase.firestore.FieldValue.increment(amount);
  return shard_ref.update('count', mutation);
};

export const useDistributedCounter = (path: string, num_shards: number = 0) => {
  const [counter, set] = useState(0);

  useEffect(() => subscribe(path, set), [path]);

  const increment = async (delta: number = 1) => {
    return mutate(path, num_shards, delta);
  };

  return [counter, increment] as const;
};
