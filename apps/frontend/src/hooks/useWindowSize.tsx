import { useEffect, useState } from "react";

export const useWindowSize = () => {
    const isClient = typeof window === 'object';
  
    function getSize() {
      return {
        width: isClient ? window.innerWidth : undefined,
        height: isClient ? window.innerHeight : undefined
      };
    }
  
    const [size, setSize] = useState(getSize);
  
    useEffect(() => {
      if (!isClient) { return; }
      
      
      const handle = () => setSize(getSize());
      window.addEventListener('resize', handle);

      return () =>  window.removeEventListener('resize', handle);
    }, []); // Empty array ensures that effect is only run on mount and unmount
  
    return size;
};

export default useWindowSize;