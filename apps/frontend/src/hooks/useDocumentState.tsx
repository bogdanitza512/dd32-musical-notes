import { useState, useEffect } from 'react';
import { db } from '../services/firebase';

export const useDocumentState = (path: string) => {
  const [state, set] = useState<firebase.firestore.DocumentData>(null);

  useEffect(
    () =>
      db.doc(path).onSnapshot(snapshot => {
        set(snapshot.data());
      }),
    [path]
  );

  const mutate = async (data: firebase.firestore.UpdateData) =>
    db.doc(path).update(data);

  return [state, mutate] as const;
};
