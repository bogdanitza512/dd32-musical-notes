import { Context } from '../context/localStore';
import { useContext } from 'react';

export const useLocalStore = () => useContext(Context);
