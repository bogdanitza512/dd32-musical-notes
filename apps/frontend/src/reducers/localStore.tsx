import { Store } from '../context/localStore';
import { ActionType as ActionOfType } from '../common/types';

export const enum Type {
  SET_NOTE_ID = 'SET_NOTE_ID',
  SET_PERFORMED_SCAN = 'SET_PERFORMED_SCAN'
}

export const actions = {
  updateNoteID: (id: number) => {
    return {
      type: Type.SET_NOTE_ID,
      id
    } as const;
  },
  updatePerformedScan: (performedScan: boolean) => {
    return {
      type: Type.SET_PERFORMED_SCAN,
      performedScan
    } as const;
  }
};

export type Action = ActionOfType<typeof actions>;

export const reducer = (state: Store, action: Action): Store => {
  switch (action.type) {
    case Type.SET_NOTE_ID:
      return { ...state, id: action.id };
    case Type.SET_PERFORMED_SCAN:
      return { ...state, performedScan: action.performedScan };
    default:
      return state;
  }
};
