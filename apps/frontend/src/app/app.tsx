import React from 'react';

import styled, { ThemeProvider } from 'styled-components';
import { BrowserRouter as Router } from 'react-router-dom';

import { theme } from '../common/theme';
import { LocalStore } from '../context/localStore';
import { Swicheroo as Switch } from '../components/switcheroo/switcheroo';

const StyledApp = styled.div`
  font-family: sans-serif;

  width: 100%;
  height: 100%;
  text-align: center;
`;

export const App = () => {
  return (
    <Router>
      <ThemeProvider theme={theme}>
        <LocalStore>
          <StyledApp>
            <Switch />
          </StyledApp>
        </LocalStore>
      </ThemeProvider>
    </Router>
  );
};

export default App;
