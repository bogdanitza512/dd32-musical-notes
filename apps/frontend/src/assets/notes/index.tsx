import React, { LegacyRef, forwardRef } from 'react';

import { ReactComponent as MN_01 } from './musical_note_01.svg';
import { ReactComponent as MN_02 } from './musical_note_02.svg';
import { ReactComponent as MN_03 } from './musical_note_03.svg';
import { ReactComponent as MN_04 } from './musical_note_04.svg';
import { ReactComponent as MN_05 } from './musical_note_05.svg';
import { ReactComponent as MN_06 } from './musical_note_06.svg';
import { ReactComponent as MN_07 } from './musical_note_07.svg';
import { ReactComponent as MN_08 } from './musical_note_08.svg';
import { ReactComponent as MN_09 } from './musical_note_09.svg';
import { ReactComponent as MN_10 } from './musical_note_10.svg';
import { ReactComponent as MN_11 } from './musical_note_11.svg';
import { ReactComponent as MN_12 } from './musical_note_12.svg';
//import { ReactComponent as MN_00 } from "./musical_note_00.svg";

export const notes = [
  {
    mp3: './assets/notes/musical_note_01.mp3',
    svg: MN_01
  },
  {
    mp3: './assets/notes/musical_note_02.mp3',
    svg: MN_02
  },
  {
    mp3: './assets/notes/musical_note_03.mp3',
    svg: MN_03
  },
  {
    mp3: './assets/notes/musical_note_04.mp3',
    svg: MN_04
  },
  {
    mp3: './assets/notes/musical_note_05.mp3',
    svg: MN_05
  },
  {
    mp3: './assets/notes/musical_note_06.mp3',
    svg: MN_06
  },
  {
    mp3: './assets/notes/musical_note_07.mp3',
    svg: MN_07
  },
  {
    mp3: './assets/notes/musical_note_08.mp3',
    svg: MN_08
  },
  {
    mp3: './assets/notes/musical_note_09.mp3',
    svg: MN_09
  },
  {
    mp3: './assets/notes/musical_note_10.mp3',
    svg: MN_10
  },
  {
    mp3: './assets/notes/musical_note_11.mp3',
    svg: MN_11
  },
  {
    mp3: './assets/notes/musical_note_12.mp3',
    svg: MN_12
  }
  // {
  //   mp3: "./assets/musical_note_00.mp3",
  //   svg: MN_00
  // }
];

export const paths = [
  {
    mp3: './assets/notes/musical_note_01.mp3',
    svg: './assets/notes/musical_note_01.svg'
  },
  {
    mp3: './assets/notes/musical_note_02.mp3',
    svg: './assets/notes/musical_note_02.svg'
  },
  {
    mp3: './assets/notes/musical_note_03.mp3',
    svg: './assets/notes/musical_note_03.svg'
  },
  {
    mp3: './assets/notes/musical_note_04.mp3',
    svg: './assets/notes/musical_note_04.svg'
  },
  {
    mp3: './assets/notes/musical_note_05.mp3',
    svg: './assets/notes/musical_note_05.svg'
  },
  {
    mp3: './assets/notes/musical_note_06.mp3',
    svg: './assets/notes/musical_note_06.svg'
  },
  {
    mp3: './assets/notes/musical_note_07.mp3',
    svg: './assets/notes/musical_note_07.svg'
  },
  {
    mp3: './assets/notes/musical_note_08.mp3',
    svg: './assets/notes/musical_note_08.svg'
  },
  {
    mp3: './assets/notes/musical_note_09.mp3',
    svg: './assets/notes/musical_note_09.svg'
  },
  {
    mp3: './assets/notes/musical_note_10.mp3',
    svg: './assets/notes/musical_note_10.svg'
  },
  {
    mp3: './assets/notes/musical_note_11.mp3',
    svg: './assets/notes/musical_note_11.svg'
  },
  {
    mp3: './assets/notes/musical_note_12.mp3',
    svg: './assets/notes/musical_note_12.svg'
  }
];

export default notes;
