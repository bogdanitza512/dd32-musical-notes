import {} from 'styled-components';
import data from '../assets/theme.json';

declare module 'styled-components' {
  type Theme = typeof theme;
  export interface DefaultTheme extends Theme {}
}

export const theme = {
  ...data,
  utils: {
    note: {
      width: () => theme.note.size.width + theme.note.size.unit,
      height: () => theme.note.size.height + theme.note.size.unit,
      offset: (amount: number) =>
        -1 * amount * theme.note.offset.scalar + theme.note.offset.unit,
      color: (active: boolean) =>
        active ? theme.note.color.active : theme.note.color.inactive
    },
    stave: {
      size: () => theme.utils.note.width() + ' ' + theme.utils.note.height()
    }
  }
};

export default theme;
