import song from '../assets/song.json';

export const getRandomNoteFromSong = () => {
  return song.score[Math.floor(Math.random() * song.score.length)];
};
